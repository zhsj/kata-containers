#!/usr/bin/make -f

include /usr/share/dpkg/default.mk
include /usr/share/rustc/architecture.mk

REPO := github.com/kata-containers/kata-containers

GO_BUILD_DIR := $(CURDIR)/_build/go
RUST_BUILD_DIR := $(CURDIR)/_build/rust
INSTALL_DIR := $(CURDIR)/debian/kata-containers

RUNTIME_BUILD_DIR := $(GO_BUILD_DIR)/src/$(REPO)/src/runtime
RUNTIME_CONF := \
	DESTDIR=$(INSTALL_DIR) \
	PREFIX=/usr \
	PKGDATADIR=/var/cache/kata-containers \
	DEFAULTSDIR=/usr/share \
	DEFVIRTIOFSDAEMON=/usr/lib/qemu/virtiofsd \
	DEFSANDBOXCGROUPONLY=true \
	SKIP_GO_VERSION_CHECK=y \
	V=1
AGENT_BUILD_DIR := $(RUST_BUILD_DIR)/src/agent

export DH_GOLANG_EXCLUDES := virtcontainers/hook/mock
export DH_GOLANG_INSTALL_EXTRA := \
	$(wildcard src/runtime/arch/*.mk) \
	$(wildcard src/runtime/config/*.toml.in) \
	VERSION \
	src/runtime/Makefile \
	src/runtime/VERSION \
	src/runtime/data/completions/bash/kata-runtime \
	src/runtime/data/kata-collect-data.sh.in \
	src/runtime/go.mod \
	src/runtime/golang.mk \
	src/runtime/pkg/katautils/config-settings.go.in

# /usr/share/perl5/Debian/Debhelper/Buildsystem/cargo.pm
export CARGO_HOME := $(RUST_BUILD_DIR)/debian/cargo_home
export DEB_CARGO_CRATE := kata-agent_0.1.0
export DEB_HOST_RUST_TYPE DEB_HOST_GNU_TYPE

%:
	dh $@ --builddirectory=_build

override_dh_auto_configure:
	# Go
	DH_GOPKG=$(REPO) dh_auto_configure --buildsystem=golang --builddirectory=$(GO_BUILD_DIR)
	# Rust
	mkdir -p $(RUST_BUILD_DIR)/src
	cp -a VERSION utils.mk pkg $(RUST_BUILD_DIR)
	cp -a src/agent $(RUST_BUILD_DIR)/src/
	find $(RUST_BUILD_DIR) -name Cargo.lock -delete
	/usr/share/cargo/bin/cargo prepare-debian $(RUST_BUILD_DIR)/debian/cargo_registry --link-from-system

override_dh_auto_build:
	# Go
	$(MAKE) -C $(RUNTIME_BUILD_DIR) $(RUNTIME_CONF) \
		pkg/katautils/config-settings.go \
		config/configuration-qemu.toml \
		data/kata-collect-data.sh
	dh_auto_build --buildsystem=golang --builddirectory=$(GO_BUILD_DIR)

override_dh_auto_install:
	# Go
	dh_auto_install --buildsystem=golang --builddirectory=$(GO_BUILD_DIR) -- --no-source
	$(MAKE) -C $(RUNTIME_BUILD_DIR) $(RUNTIME_CONF) \
		install-scripts install-completions install-configs
	# Rust
	-cd $(AGENT_BUILD_DIR) && env DESTDIR=$(INSTALL_DIR)/usr/libexec/kata-containers/agent \
		/usr/share/cargo/bin/cargo install

override_dh_auto_test:
	# Go
	-dh_auto_test --buildsystem=golang --builddirectory=$(GO_BUILD_DIR)
	# Rust
	-cd $(AGENT_BUILD_DIR) && /usr/share/cargo/bin/cargo test

execute_before_dh_gencontrol:
	# GO
	dh_golang --builddirectory=$(GO_BUILD_DIR)
	# Rust
	-cd $(AGENT_BUILD_DIR) && /usr/share/cargo/bin/dh-cargo-built-using kata-containers

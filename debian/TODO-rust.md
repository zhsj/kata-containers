```bash
find -name Cargo.toml|xargs  grep --no-filename -P ' = ("|\{)'|sort -u|grep -vP '^(authors|name|members|version|edition)|(path =)'

```

| package               | version in Debian | version in Cargo.toml    |
| --------------------- | ----------------- | ------------------------ |
| anyhow                | 1.0.44            | "1.0.32"                 |
| async-recursion       |                   | "0.3.2"                  |
| async-trait           | 0.1.24            | "0.1.50"                 |
| bincode               | 1.3.3             | "1.3.3"                  |
| byteorder             | 1.4.3             | "1.4.3"                  |
| capctl                |                   | "0.2.0"                  |
| caps                  | 0.3.3             | "0.5.0"                  |
| cgroups-rs            |                   | "0.2.5"                  |
| futures               | 0.1.29            | "0.3.12"                 |
| inotify               |                   | "0.9.2"                  |
| ipnetwork             | 0.17.0            | "0.17.0"                 |
| lazy_static           | 1.4.0             | "1.3.0"                  |
| libc                  | 0.2.103           | "0.2.94"                 |
| libseccomp            |                   | "0.1.3", optional = true |
| log                   | 0.4.11            | "0.4.11"                 |
| netlink-packet-utils  |                   | "0.4.1"                  |
| netlink-sys           |                   | "0.7.0"                  |
| nix                   | 0.23.0            | "0.21.0"                 |
| opentelemetry         |                   | "0.14.0"                 |
| path-absolutize       |                   | "1.2.0"                  |
| procfs                |                   | "0.7.9"                  |
| prometheus            |                   | "0.9.0"                  |
| protobuf              | 2.25.2            | "=2.14.0"                |
| regex                 | 1.5.4             | "1.1"                    |
| rlimit                |                   | "0.5.3"                  |
| rtnetlink             |                   | "0.8.0"                  |
| scan_fmt              |                   | "0.2.3"                  |
| scopeguard            | 1.1.0             | "1.0.0"                  |
| serde                 | 1.0.130           | "1.0.129"                |
| serde_derive          | 1.0.130           | "1.0.91"                 |
| serde_json            | 1.0.41            | "1.0.39"                 |
| serial_test           | 0.5.1             | "0.5.1"                  |
| slog                  | 2.5.2             | "2.5.2"                  |
| slog-async            | 2.5.0             | "2.3.0"                  |
| slog-json             |                   | "2.3.0"                  |
| slog-scope            |                   | "4.1.2"                  |
| slog-stdlog           |                   | "4.0.0"                  |
| tempfile              | 3.1.0             | "3.1.0"                  |
| thiserror             | 1.0.20            | "1.0.26"                 |
| tokio                 | 1.13.0            | "1.2.0"                  |
| tokio-vsock           | 0.3.1             | "0.3.1"                  |
| toml                  | 0.5.8             | "0.5.8"                  |
| tracing               |                   | "0.1.26"                 |
| tracing-opentelemetry |                   | "0.13.0"                 |
| tracing-subscriber    |                   | "0.2.18"                 |
| ttrpc                 | 0.5.2             | "0.5.0"                  |
| ttrpc-codegen         |                   | "0.2.0"                  |

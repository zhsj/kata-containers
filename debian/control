Source: kata-containers
Maintainer: Shengjing Zhu <zhsj@debian.org>
Uploaders: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>,
           Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>,
Section: admin
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: cargo:native,
               debhelper-compat (= 13),
               dh-cargo,
               dh-golang,
               golang-any,
               golang-github-blang-semver-dev,
               golang-github-burntsushi-toml-dev,
               golang-github-containerd-cgroups-dev,
               golang-github-containerd-console-dev,
               golang-github-containerd-containerd-dev,
               golang-github-containerd-fifo-dev,
               golang-github-containerd-ttrpc-dev,
               golang-github-containerd-typeurl-dev,
               golang-github-containernetworking-plugins-dev,
               golang-github-fsnotify-fsnotify-dev,
               golang-github-go-ini-ini-dev,
               golang-github-go-openapi-errors-dev,
               golang-github-go-openapi-runtime-dev,
               golang-github-go-openapi-strfmt-dev,
               golang-github-go-openapi-swag-dev,
               golang-github-go-openapi-validate-dev,
               golang-github-gogo-protobuf-dev,
               golang-github-hashicorp-go-multierror-dev,
               golang-github-intel-go-cpuid-dev,
               golang-github-kata-containers-govmm-dev,
               golang-github-mdlayher-vsock-dev,
               golang-github-opencontainers-runc-dev,
               golang-github-opencontainers-selinux-dev,
               golang-github-opencontainers-specs-dev,
               golang-github-pkg-errors-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-prometheus-client-model-dev,
               golang-github-prometheus-common-dev,
               golang-github-prometheus-procfs-dev,
               golang-github-safchain-ethtool-dev,
               golang-github-sirupsen-logrus-dev,
               golang-github-stretchr-testify-dev,
               golang-github-urfave-cli-dev,
               golang-github-vishvananda-netlink-dev,
               golang-github-vishvananda-netns-dev,
               golang-golang-x-net-dev,
               golang-golang-x-oauth2-dev,
               golang-golang-x-sys-dev,
               golang-google-grpc-dev,
               golang-gopkg-inf.v0-dev,
               golang-opentelemetry-otel-dev,
               librust-anyhow-dev,
               librust-async-trait-dev,
               librust-bincode-dev,
               librust-byteorder-dev,
               librust-caps-dev,
               librust-futures-dev,
               librust-ipnetwork-dev,
               librust-lazy-static-dev,
               librust-libc-dev,
               librust-log-dev,
               librust-nix-dev,
               librust-protobuf-dev,
               librust-regex-dev,
               librust-scopeguard-dev,
               librust-serde-derive-dev,
               librust-serde-dev,
               librust-serde-json-dev,
               librust-serde-test-dev,
               librust-slog-async-dev,
               librust-slog-dev,
               librust-tempfile-dev,
               librust-thiserror-dev,
               librust-tokio-dev,
               librust-tokio-vsock-dev,
               librust-toml-dev,
               librust-ttrpc-dev,
               libstd-rust-dev,
               rustc:native,
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/zhsj/kata-containers
Vcs-Git: https://salsa.debian.org/zhsj/kata-containers.git
Homepage: https://github.com/kata-containers/kata-containers
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/kata-containers/kata-containers/src/runtime

Package: kata-containers
Architecture: amd64 arm64 ppc64el s390x
Depends: qemu-system-arm [arm64],
         qemu-system-misc [s390x],
         qemu-system-ppc [ppc64el],
         qemu-system-x86 [amd64],
         ${misc:Depends},
         ${shlibs:Depends},
Built-Using: ${cargo:Built-Using},
             ${misc:Built-Using},
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: secure container runtime with lightweight virtual machines
 Kata Containers is an open source project and community working to build a
 standard implementation of lightweight Virtual Machines (VMs) that feel and
 perform like containers, but provide the workload isolation and security
 advantages of VMs.
